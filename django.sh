#!/bin/bash

#########       BASIC QUIERIS   #####################
echo "Do you want to Install dependencies ? (y/n)"
read Install
DoInstall="$(echo -n "${Install}" | sed -e 's/[^[:alnum:]]/_/g' | tr -s '-' | tr A-Z a-z)"

echo "Do you want to Install Basic PIP packages ? (y/n)"
read PipInstall
DoPipInstall="$(echo -n "${PipInstall}" | sed -e 's/[^[:alnum:]]/_/g' | tr -s '-' | tr A-Z a-z)"

echo "Enter your gitlab username: (example: zatha)"
read gitlabuser
GitUser="$(echo -n ${gitlabuser})"

echo "Enter your gitlab project name:"
read gitlabproject
GitProject="$(echo -n ${gitlabproject})"

###########     Install Dependencies    ####################
if [ "$DoInstall" == "y" ]
    then
        sudo apt-get update
        sudo apt-get install git
        sudo apt-get install python3-pip
        sudo apt-get install build-essential libssl-dev libpq-dev libcurl4-gnutls-dev libexpat1-dev gettext unzip
        sudo apt-get install postgresql postgresql-contrib
        sudo apt-get install apache2
        sudo pip3 install virtualenv
fi

############    Initialize Git Project  ################
#mkdir ~/${GitUser} && cd ~/${GitUser}/
mkdir ~/${GitProject} && cd ${GitProject}/
git clone https://gitlab.com/${GitUser}/${GitProject}.git

###########     Postgres Db setup   ############
sudo -u postgres psql --command="ALTER USER postgres WITH PASSWORD 'postgres';"
DB_NAME=${GitProject}_db
sudo -u postgres psql --command="CREATE DATABASE ${DB_NAME};"
sudo -u postgres psql --command="ALTER ROLE postgres SET client_encoding TO 'utf8';"
sudo -u postgres psql --command="ALTER ROLE postgres SET default_transaction_isolation TO 'read committed';"
sudo -u postgres psql --command="ALTER ROLE postgres SET timezone TO 'Asia/Kolkata';"
sudo -u postgres psql --command="GRANT ALL PRIVILEGES ON DATABASE ${DB_NAME} TO postgres;"
###########     Install PIP Packages    ###############
virtualenv -p python3 env
source ~/${GitProject}/env/bin/activate
if [ "$DoPipInstall" == "y" ]
    then
        pip install django==2.2.2 djangorestframework djangorestframework-jwt psycopg2 django-filter Markdown pillow django-cors-headers
fi

###########     Project setup   ################
cd ${GitProject}
django-admin startproject ${GitProject} .
mkdir static
mkdir media
mkdir templates
cd ${GitProject}

###   settings.py
sed -i '57d' settings.py
sed -i "57i\ \t'DIRS':[os.path.join(BASE_DIR,'templates')]," settings.py
sed -i '78,79d' settings.py
sed -i "78i\ \t'ENGINE': 'django.db.backends.postgresql_psycopg2'," settings.py
sed -i "79i\ \t'NAME': '${DB_NAME}'," settings.py
sed -i "80i\ \t'USER': 'postgres'," settings.py
sed -i "81i\ \t'PASSWORD': 'postgres'," settings.py
sed -i "82i\ \t'HOST': 'localhost'," settings.py
sed -i "83i\ \t'PORT': 5432," settings.py
sed -i "$ a #################     Additional Setup    #############" settings.py
sed -i "$ a STATIC_ROOT='static'" settings.py
sed -i "$ a MEDIA_URL = '/media/'\nMEDIA_ROOT = os.path.join(BASE_DIR,'media')" settings.py

#########   urls.py
sed -i "18i\from django.conf import settings" urls.py
sed -i "19i\from django.conf.urls.static import static" urls.py
sed -i '$d' urls.py
sed -i "$ a ] + static(settings.STATIC_URL,document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)" urls.py

############    Run Project     ################
cd ..
python manage.py migrate
python manage.py collectstatic
