#! /usr/bin/python3
import os,sys
import socket
import platform

#colour codings
blue = '\033[94m'
green = '\033[92m'
red = '\033[91m'
end = '\033[0m'
bold = '\033[1m'
dim = '\033[2m'

#system details
os_version = str(platform.dist()[2])

#Basic information
print("\n{g}{b}Welcome! It is a python script for setup django project...{e}\n".format(g=green,b=bold,e=end))
print('{b}Fillup your requirements with choice...{e}\n'.format(e=end,b=blue))

#checking connection ()
def is_connected(hostname):
	try:
		host = socket.gethostbyname(hostname)
		s = socket.create_connection((host, 80), 2)
		s.close()
		return True
	except:
		pass
	return False
net = is_connected("www.google.com")

# user choice ()
def user_choice(ch_text,ch1=None, ch2=None, ch3=None, ch4=None, ch5=None):
	choice = [0, 1, 2, 3, 4, 5]
	print("{b}--------------{ch_text}---------------{e}".format(b=blue,e=end,ch_text=ch_text))
	print('{r}(0) exit (stop the process){e}'.format(e=end,r=red))
	if ch1 != None:
		print("{b}(1){e} {ch1}".format(ch1=ch1,b=blue,e=end))
	if ch2 != None:	
		print("{b}(2){e} {ch2}".format(ch2=ch2,b=blue,e=end))
	if ch3 != None:
		print("{b}(3){e} {ch3}".format(ch3=ch3,b=blue,e=end))
	if ch4 != None:
		print("{b}(4){e} {ch4}".format(ch4=ch4,b=blue,e=end))
	if ch5 != None:
		print("{b}(5){e} {ch5}".format(ch5=ch5,b=blue,e=end))
	while True: 
		try:
			ch = int(input('--> '))
			if ch in choice:
				if ch == 0:
					print("Bye ...")
					sys.exit()	
				break
			else:
				print("{r}invalid choice{e}".format(r=red,e=end))
		except Exception:
			print("Enter valid {g}integer{e} choice !".format(g=green,e=end))
	return ch

# postgresql installation ()
def postgres(os_version):
	try:
		os.system('touch pgdg.list')
		if os_version == "xenial":
			os.system('echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" >> pgdg.list')
			os.system('sudo cp pgdg.list /etc/apt/sources.list.d/')
			os.system('rm pgdg.list')
		elif os_version == "bionic":
			os.system('echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" >> pgdg.list')
			os.system('sudo cp pgdg.list /etc/apt/sources.list.d/')
			os.system('rm pgdg.list')
		os.system('wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -')
		os.system('sudo apt-get update')
		os.system('sudo apt-get install postgresql postgresql-contrib libpq-dev postgresql-server-dev -y')
	except Exception as e:
		print("Some issues in pgdb_install",e)
	
# install dependicies ()
def app_install(*args,**kwargs):
	if net:	
		os.system('sudo apt-get update')
		os.system('sudo apt-get install python3-pip')
		os.system('sudo pip3 install virtualenv')
		if database_choice == 1:
			postgres(os_version)
		elif database_choice == 2:
			mysql(os_version)
		elif database_choice == 3:
			mongodb(os_version)
		if apache_choice == 1:
			os.system('sudo apt-get install apache2')
	else:
		print("Check your internet connection !")
		
	

# basic quries
proj_type = user_choice("Project type","local setup", "use existing project in git repo")
apache_choice = user_choice("Apache2 server configuration","Yes","No")
database_choice = user_choice("Database setup","Postgres","MySql","MongoDB","Not now")





