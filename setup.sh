echo "Hi i'm ---zatha--- It is a shell scripting for create a blank django project"
echo "You want to install pip and other requirements (y/n)"
read Install
setupInstall="$(echo -n "${Install}" | sed -e 's/[^[:alnum:]]/_/g' | tr -s '-' | tr A-Z a-z)"
echo "You want to install postgresql (y/n)"
read postgresqlInstall
setupPostgresql="$(echo -n "${postgresqlInstall}" | sed -e 's/[^[:alnum:]]/_/g' | tr -s '-' | tr A-Z a-z)"
echo "You want to install git (y/n)"
read gitInstall
setupGit="$(echo -n "${gitInstall}" | sed -e 's/[^[:alnum:]]/_/g' | tr -s '-' | tr A-Z a-z)"
echo -n "Enter your project name:"
read projectname
ProjectName="$(echo -n "${projectname}" | sed -e 's/[^[:alnum:]]/_/g' | tr -s '-' | tr A-Z a-z)"
echo "Using ${projectname} into ${ProjectName} (formatted automatically)"
if [ "$setupInstall" == "y" ]
    then
        echo "Installing Dependencies..."
        sudo apt-get update
        sudo apt-get install python3-pip
        sudo pip3 install virtualenv
fi
if [ "$setupPostgresql" == 'y' ]
    then
        sudo apt-get update
        sudo apt-get install postgresql postgresql-contrib libpq-dev
        sudo -u postgres psql --command="ALTER USER postgres WITH PASSWORD 'postgres';"
fi
if [ "$setupGit" == 'y' ]
    then
        sudo apt-get install git
DB_NAME="${ProjectName}_db"
sudo -u postgres psql --command="CREATE DATABASE ${DB_NAME};"
mkdir ~/${ProjectName}
cd ~/${ProjectName}/
virtualenv -p python3 env
source ~/${ProjectName}/env/bin/activate
pip install django==2.2.2
pip install psycopg2
django-admin startproject ${ProjectName}
cd ~/${ProjectName}/${ProjectName}
##add git installation
sed -i '78,79d' ~/${ProjectName}/${ProjectName}/${ProjectName}/settings.py
sed -i "78i\ \t'ENGINE': 'django.db.backends.postgresql_psycopg2'," ~/${ProjectName}/${ProjectName}/${ProjectName}/settings.py
sed -i "79i\ \t'NAME': '${DB_NAME}'," ~/${ProjectName}/${ProjectName}/${ProjectName}/settings.py
sed -i "80i\ \t'USER': 'postgres'," ~/${ProjectName}/${ProjectName}/${ProjectName}/settings.py
sed -i "81i\ \t'PASSWORD': 'postgres'," ~/${ProjectName}/${ProjectName}/${ProjectName}/settings.py
sed -i "82i\ \t'HOST': 'localhost'," ~/${ProjectName}/${ProjectName}/${ProjectName}/settings.py
sed -i "83i\ \t'PORT': 5432," ~/${ProjectName}/${ProjectName}/${ProjectName}/settings.py
python manage.py migrate
# python manage.py createsuperuser
# code ~/${ProjectName}/${ProjectName}
